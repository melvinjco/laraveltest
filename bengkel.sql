--
-- Struktur dari tabel `adm`
--

CREATE TABLE IF NOT EXISTS `adm` (
  `idadm` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL,
  PRIMARY KEY (`idadm`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `adm`
--

INSERT INTO `adm` VALUES('dago', 'dago');
INSERT INTO `adm` VALUES('juanda', 'juanda');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detilwo`
--

CREATE TABLE IF NOT EXISTS `detilwo` (
  `nowo` int(11) NOT NULL,
  `idservis` char(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detilwo`
--

INSERT INTO `detilwo` VALUES(1, '01');
INSERT INTO `detilwo` VALUES(1, '02');
INSERT INTO `detilwo` VALUES(1, '03');
INSERT INTO `detilwo` VALUES(2, '02');
INSERT INTO `detilwo` VALUES(2, '04');
INSERT INTO `detilwo` VALUES(3, '05');
INSERT INTO `detilwo` VALUES(4, '02');
INSERT INTO `detilwo` VALUES(4, '03');
INSERT INTO `detilwo` VALUES(4, '04');
INSERT INTO `detilwo` VALUES(5, '01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `montir`
--

CREATE TABLE IF NOT EXISTS `montir` (
  `idmontir` char(2) NOT NULL,
  `nama` varchar(20) NOT NULL,
  PRIMARY KEY (`idmontir`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `montir`
--

INSERT INTO `montir` VALUES('TS', 'Trisno Sumarjo');
INSERT INTO `montir` VALUES('BP', 'Brad Pitt');
INSERT INTO `montir` VALUES('SM', 'Sony Mulyana');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tarif`
--

CREATE TABLE IF NOT EXISTS `tarif` (
  `idservis` char(2) NOT NULL,
  `ketservis` varchar(20) NOT NULL,
  `tarifservis` int(11) NOT NULL,
  PRIMARY KEY (`idservis`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tarif`
--

INSERT INTO `tarif` VALUES('01', 'Tune Up', 45000);
INSERT INTO `tarif` VALUES('02', 'Ganti Olie', 5000);
INSERT INTO `tarif` VALUES('03', 'Ganti Kanvas Rem', 7500);
INSERT INTO `tarif` VALUES('04', 'Cek Kelistrikan', 63000);
INSERT INTO `tarif` VALUES('05', 'Overhaul', 150000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `workorder`
--

CREATE TABLE IF NOT EXISTS `workorder` (
  `nowo` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `idmontir` char(2) NOT NULL,
  `nomotor` varchar(10) NOT NULL,
  PRIMARY KEY (`nowo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `workorder`
--

INSERT INTO `workorder` VALUES(1, '2017-05-01', 'SM', 'D-1122-YS');
INSERT INTO `workorder` VALUES(2, '2017-05-01', 'TS', 'D-8765-DX');
INSERT INTO `workorder` VALUES(3, '2017-05-03', 'TS', 'B-1001-HZ');
INSERT INTO `workorder` VALUES(4, '2017-05-05', 'BP', 'D-8989-KL');
INSERT INTO `workorder` VALUES(5, '2017-05-09', 'SM', 'D-8050-CR');
