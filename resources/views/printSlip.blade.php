<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Slip Upah</title>

    <!--Bootstrap-->

    <link href="{{ asset('css/sb-admin-2.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap-theme.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/dataTables.bootstrap.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/dataTables.responsive.css') }}" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('js/html5shiv.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-1.11.3.js') }}"></script>
    {{--    <link href="{{ asset('css/font-awesome.css') }}" media="all" rel="stylesheet" type="text/css" />--}}
    <link href="{{ asset('css/font-awesome.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    {{--<link href="{{ asset('css/metisMenu.min.css') }}" media="all" rel="stylesheet" type="text/css" />--}}
    {{--<link href="{{ asset('css/morris.css') }}" media="all" rel="stylesheet" type="text/css" />--}}

</head>

<body>

<!-- Display -->

@include('include.sidebar')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Slip Upah Montir</h1>
            <div class="col-lg-9">
                <div class="panel panel-default">
                    <div class="panel-heading">

                    </div>
                    <div class="panel-body">
                        <form action="" class="form-horizontal" method="get">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label for="idmontir" class="control-label col-lg-2">ID Montir :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" disabled value="<?php echo $printSlip[0]->idmontir?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="control-label col-lg-2">Nama :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" disabled name="nama" id="nama" value="{{$nama}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tgl_awal" class="control-label col-lg-2">Tgl Awal :</label>
                                <div class="col-lg-9">
                                    <input type="date" class="form-control" disabled name="tgl_awal" id="tgl_awal" value="{{$tgl_awal}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tgl_akhir" class="control-label col-lg-2">Tgl Akhir :</label>
                                <div class="col-lg-9">
                                    <input type="date" class="form-control" disabled name="tgl_akhir" id="tgl_akhir" value="{{$tgl_akhir}}">
                                </div>
                            </div>
                            <hr>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>No Urut</th>
                                    <th>Tanggal</th>
                                    <th>No WO</th>
                                    <th>Nama Servis</th>
                                    <th>Upah</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{$nourut = ""}}
                                @foreach($printSlip as $item)
                                    <tr>
                                        <td>{{$nourut+=1}}</td>
                                        <td>{{$item->tgl}}</td>
                                        <td>{{$item->nowo}}</td>
                                        <td>{{$item->ketservis}}</td>
                                        <td>{{$item->upah}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </form>
                    </div> <!-- End panel body -->
                </div> <!-- End panel default -->
            </div>
        </div>
    </div>
</div>

</body>

<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/dataTables.responsive.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/metisMenu.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sb-admin-2.js') }}"></script>
</html>