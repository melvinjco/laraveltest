<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Melvin App</title>
    <link href="{{ asset('css/sb-admin-2.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap-theme.css') }}" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('js/html5shiv.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-1.11.3.js') }}"></script>
    <link href="{{ asset('css/font-awesome.css') }}" media="all" rel="stylesheet" type="text/css" />
</head>

<body data-spy="scroll" data-target="#my-navbar">

<!--navbar-->
<nav class="navbar navbar-inverse navbar-fixed-top" id="my-navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="" class="navbar-brand">PT. Jaya Abadi</a>
        </div> <!-- Navbar Header -->

        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="#feedback"> Feedback</a></li>
                <li><a href="#gallery">Gallery</a></li>
                <li><a href="#features">Features</a></li>
                <li><a href="#faq">FAQ</a></li>
                <li><a href="#contact">Contact Us</a></li>
            </ul>
            <!--Log In-->
            <a href="{{route('login')}}" class="btn btn-info navbar-btn navbar-right">Log In</a>
        </div>

    </div> <!-- End Container -->
</nav> <!-- End Navbar -->

<!-- jumbotron -->
<div class="jumbotron">
    <div class="container text-center" style="padding-top: 10px;">
        <h1>PT. Jaya Abadi</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum numquam, alias neque? Eius consequuntur.</p>
    </div> <!-- End Container -->
</div> <!-- End Jumbotron -->

<!-- Feedback -->
<div class="container">
    <section>
        <div class="page-header" id="feedback">
            <h2>Feedback. <small>Check Out Feedback From Our Users</small></h2>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium odio fugiat, cumque minima et ut explicabo iure assumenda dolores harum quod sunt alias neque corporis voluptate, sed eligendi dicta incidunt.</p>
                    <footer>Melvin Jeconiah</footer>
                </blockquote>
            </div>
            <div class="col-lg-4">
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium odio fugiat, cumque minima et ut explicabo iure assumenda dolores harum quod sunt alias neque corporis voluptate, sed eligendi dicta incidunt.</p>
                    <footer>Melvin Jeconiah</footer>
                </blockquote>
            </div>
            <div class="col-lg-4">
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium odio fugiat, cumque minima et ut explicabo iure assumenda dolores harum quod sunt alias neque corporis voluptate, sed eligendi dicta incidunt.</p>
                    <footer>Melvin Jeconiah</footer>
                </blockquote>
            </div>
        </div> <!-- End Row -->
    </section>
</div> <!-- End Container -->

<!--Subscribe call to action-->
<section>
    <div class="well">
        <div class="container text-center">
            <h3>Subscribe for more free stuff</h3>
            <h5>Enter Your Name and E-mail</h5>

            <form action="" class="form-inline">
                <div class="form-group">
                    <label for="subscribe">Name &nbsp : &nbsp</label>
                    <input type="text" id="subscribe" class="form-control" placeholder="Enter Name">&nbsp &nbsp
                </div>
                <div class="form-group">
                    <label for="email">E-mail &nbsp : &nbsp</label>
                    <input type="text" id="email" class="form-control" placeholder="Enter E-mail">&nbsp &nbsp
                </div>
                <button type="button" class="btn btn-info">Submit</button>
            </form>
            <hr>
        </div> <!-- End Container -->
    </div> <!-- End Well -->
</section>

<!-- Gallery -->
<div class="container">
    <section>
        <div class="page-header" id="gallery">
            <h2>Gallery. <small>Our Activity</small></h2>
        </div>

        <div class="carousel slide" id="screenshot-carousel" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#screenshot-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#screenshot-carousel" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active">
                    <img src="http://4.bp.blogspot.com/-r4SyX4d3R74/TYN_uwOXx_I/AAAAAAAABZo/y3ccLCgeJB0/s1600/AdSense%2BPaddy%2527s%2Bteam%2Bphoto-6.jpg" alt="activity1">
                    <div class="carousel-caption">
                        <h3>Together We Can</h3>
                        <p>This is caption</p>
                    </div>
                </div>
                <div class="item">
                    <img src="http://press-office.holidayextras.co.uk/wp-content/uploads/2012/02/hx-api-feb20121.jpg" alt="activity2">
                    <div class="carousel-caption">
                        <h3>Proud of Us</h3>
                        <p>This is caption</p>
                    </div>
                </div>
            </div>

            <a href="#screenshot-carousel" class="left carousel-control" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a href="#screenshot-carousel" class="right carousel-control" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div> <!-- End Carousel -->
    </section>
</div>

<hr>

<!--Features-->
<div class="container">
    <section>

        <div class="page-header" id="features">
            <h2>Features. <small>Check out the Awesome Features</small></h2>
        </div>

        <div class="row">
            <div class="col-md-8">
                <h3>This is Heading</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
            </div>
            <div class="col-md-4">
                <img src="" class="img-responsive" alt="features1">
            </div>
        </div> <!-- End Row -->
        <div class="row">
            <div class="col-md-8">
                <h3>This is Heading</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
            </div>
            <div class="col-md-4">
                <img src="" class="img-responsive" alt="features2">
            </div>
        </div> <!-- End Row -->

        <hr>

        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default text-center">
                    <div class="panel-body">
                        <span class="glyphicon glyphicon-star"></span>
                        <h3>This is Heading</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-default text-center">
                    <div class="panel-body">
                        <span class="glyphicon glyphicon-star"></span>
                        <h3>This is Heading</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-default text-center">
                    <div class="panel-body">
                        <span class="glyphicon glyphicon-star"></span>
                        <h3>This is Heading</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
            </div>


        </div> <!-- End Row -->

    </section>
</div> <!-- End container -->

<!-- FAQ -->
<div class="container">
    <section>
        <div class="page-header" id="faq">
            <h2>FAQ. <small>Engaging with Users</small></h2>
        </div>

        <div class="panel-group" id="accordion">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <a href="#collapse-1" data-toggle="collapse" data-parent="#accordion">Question One?</a>
                    </div> <!-- End panel title -->

                    <div id="collapse-1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <a href="#collapse-2" data-toggle="collapse" data-parent="#accordion">Question Two?</a>
                    </div> <!-- End panel title -->

                    <div id="collapse-2" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <a href="#collapse-3" data-toggle="collapse" data-parent="#accordion">Question Three?</a>
                    </div> <!-- End panel title -->

                    <div id="collapse-3" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div> <!-- End container -->

<!-- Contact Us -->
<div class="container">
    <section>
        <div class="page-header" id="contact">
            <h2>Contact. <small>Contact us for more</small></h2>
        </div> <!-- End Page Header -->

        <div class="row">
            <div class="col-md-4">
                <p>Send us a message, or contact us below</p>

                <address>
                    <strong>Vin Web Developer</strong> </br>
                    Maleber St. Number 3 </br>
                    West Bandung - 40184 </br>
                    Phone : 085722554444
                </address>
            </div>

            <div class="col-md-8">
                <form action="" class="form-horizontal">

                    <div class="form-group">
                        <label for="user-name" class="col-md-2 control-label">
                            Name
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="user-name" placeholder="Enter Your Name">
                        </div>
                    </div> <!-- End Form Group -->

                    <div class="form-group">
                        <label for="email" class="col-md-2 control-label">
                            Email
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="email" placeholder="Enter Your Email Address">
                        </div>
                    </div> <!-- End Form Group -->

                    <div class="form-group">
                        <label for="url" class="col-md-2 control-label">
                            Website
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="url" placeholder="If You have a website">
                        </div>
                    </div> <!-- End Form Group -->

                    <div class="form-group">
                        <label for="user-message" class="col-md-2 control-label">Any Message</label>
                        <div class="col-md-10">
                            <textarea name="user-message" id="user-message" class="form-control" cols="85" rows="10" placeholder="Enter a message"></textarea>
                        </div>
                    </div> <!-- End Form Group -->

                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div> <!-- End Row -->
    </section>
</div>

<!-- Footer -->
<footer>
    <hr>
    <div class="container text-center">
        <h3>Subscribe for more free stuff</h3>
        <h5>Enter Your Name and E-mail</h5>

        <form action="" class="form-inline">
            <div class="form-group">
                <label for="subscribe">Name &nbsp : &nbsp</label>
                <input type="text" id="subscribe" class="form-control" placeholder="Enter Name">&nbsp &nbsp
            </div>
            <div class="form-group">
                <label for="email">E-mail &nbsp : &nbsp</label>
                <input type="text" id="email" class="form-control" placeholder="Enter E-mail">&nbsp &nbsp
            </div>
            <button type="button" class="btn btn-info">Submit</button>
        </form>
        <hr>

        <ol class="list-inline">
            <li><a href="www.facebook.com" data-toggle="popover" title="facebook">Facebook</a></li>
            <li><a href="www.twitter.com">Twitter</a></li>
            <li><a href="www.youtube.com">Youtube</a></li>
        </ol>

    </div> <!-- End Container -->
</footer>

<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/metisMenu.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sb-admin-2.js') }}"></script>

</body>
</html>

