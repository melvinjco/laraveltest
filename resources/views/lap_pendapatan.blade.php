<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Laporan Pendapatan</title>

    <!--Bootstrap-->

    <link href="{{ asset('css/sb-admin-2.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap-theme.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/dataTables.bootstrap.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/dataTables.responsive.css') }}" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('js/html5shiv.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-1.11.3.js') }}"></script>
{{--    <link href="{{ asset('css/font-awesome.css') }}" media="all" rel="stylesheet" type="text/css" />--}}
        <link href="{{ asset('css/font-awesome.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    {{--<link href="{{ asset('css/metisMenu.min.css') }}" media="all" rel="stylesheet" type="text/css" />--}}
    {{--<link href="{{ asset('css/morris.css') }}" media="all" rel="stylesheet" type="text/css" />--}}

</head>

<body>
@if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('message'))
    <div class="alert alert-success">
        {{Session::get('message')}}
    </div>
@endif

<!-- Display -->

@include('include.sidebar')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Laporan Pendapatan</h1>
            <div class="col-lg-9">
                <div class="panel panel-default">
                    <div class="panel-heading">

                    </div>
                    <div class="panel-body">
                        <form action="{{route('lap_pendapatan')}}" class="form-horizontal" method="get">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label for="tgl_awal" class="control-label col-lg-2">Tgl Awal :</label>
                                <div class="col-lg-9">
                                    <input type="date" class="form-control" name="tgl_awal" id="tgl_awal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tgl_akhir" class="control-label col-lg-2">Tgl Akhir :</label>
                                <div class="col-lg-9">
                                    <input type="date" class="form-control" name="tgl_akhir" id="tgl_akhir">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <div class="col-lg-offset-5">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                    &nbsp;&nbsp;
                                    <input type="reset" class="btn btn-danger" value="Reset">
                                </div>
                            </div>

                            <hr>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>No. Workorder</th>
                                    <th>Tanggal</th>
                                    <th>No. Motor</th>
                                    <th>Jumlah Pendapatan</th>
                                    <th>Detil</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{$total = ''}}
                                @foreach($lap_pendapatan as $item)
                                    <tr>
                                        <td>{{$item->nowo}}</td>
                                        <td>{{$item->tgl}}</td>
                                        <td>{{$item->nomotor}}</td>
                                        <td>{{$item->jumlahPendapatan}}</td>
                                        <td><a href="/printDetil/{{$item->nowo}}&{{request('tgl_awal')}}&{{request('tgl_akhir')}}">Print</a></td>
                                        <?php $total += $item->jumlahPendapatan ?>
                                    </tr>
                                @endforeach
                                    <tr>
                                        <td colspan="3">Total Upah :</td>
                                        <td colspan="2">{{$total}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div> <!-- End panel body -->
                </div> <!-- End panel default -->
            </div>
        </div>
    </div>
</div>

</body>

<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/dataTables.responsive.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/metisMenu.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sb-admin-2.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
</html>