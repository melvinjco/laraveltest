<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Entri Work Order</title>

    <!--Bootstrap-->

    <link href="{{ asset('css/sb-admin-2.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap-theme.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/dataTables.bootstrap.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/dataTables.responsive.css') }}" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('js/html5shiv.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-1.11.3.js') }}"></script>
{{--    <link href="{{ asset('css/font-awesome.css') }}" media="all" rel="stylesheet" type="text/css" />--}}
    <link href="{{ asset('css/font-awesome.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    {{--<link href="{{ asset('css/metisMenu.min.css') }}" media="all" rel="stylesheet" type="text/css" />--}}
    {{--<link href="{{ asset('css/morris.css') }}" media="all" rel="stylesheet" type="text/css" />--}}

</head>

<body>
@if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('message'))
    <div class="alert alert-success">
        {{Session::get('message')}}
    </div>
@endif

<!-- Display -->

@include('include.sidebar')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Data Workorder</h1>
            <div class="col-lg-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Input Workorder
                    </div>
                    <div class="panel-body">
                        {{--detil_workorder--}}
                        <form action="{{route('saveServis')}}" class="form-horizontal" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label for="idservis" class="control-label col-lg-2">ID Servis :</label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" name="idservis" id="idservis">
                                    <input type="text" hidden value="<?php echo $workorder[0]->nowo ?>" name="nowo">
                                    <br>
                                    <input type="submit" class="btn btn-block btn-primary" value="Save ID Servis">
                                </div>
                                <div class="col-lg-7">
                                    <table width="100%" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID Servis</th>
                                                <th>Keterangan</th>
                                                <th>Tarif</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($tarif as $item)
                                            @if($item->nowo <> $workorder[0]->nowo)
                                                <tr></tr>
                                            @else
                                            <tr>
                                                <td>{{$item->idservis}}</td>
                                                <td>{{$item->ketservis}}</td>
                                                <td>{{$item->tarifservis}}</td>
                                            </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr>
                        </form>
                        {{--workorder--}}
                        <form action="{{route('saveWo')}}" class="form-horizontal" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label for="nowo" class="control-label col-lg-2">No. WO :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="nowo" id="nowo" value="<?php echo $workorder[0]->nowo ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tgl" class="control-label col-lg-2">Tanggal :</label>
                                <div class="col-lg-9">
                                    <input type="date" class="form-control" name="tgl" id="tgl" value="<?php echo $workorder[0]->tgl?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="idmontir" class="control-label col-lg-2">Montir :</label>
                                <div class="col-lg-9">
                                    <select name="idmontir" id="idmontir" class="form-control">
                                        @foreach($montir as $item)
                                            <option>{{$item->nama}} ( {{$item->idmontir}} )</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nomotor" class="control-label col-lg-2">No. Motor :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="nomotor" id="nomotor">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <div class="col-lg-offset-4">
                                    <input type="submit" class="btn btn-primary" value="Save Workorder">
                                    &nbsp;&nbsp;
                                    <input type="reset" class="btn btn-danger" value="Reset Workorder">
                                </div>
                            </div>
                            <hr>
                        </form>
                    </div> <!-- End panel body -->
                </div> <!-- End panel default -->
            </div>
        </div>
    </div>
</div>

</body>

<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/dataTables.responsive.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/metisMenu.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sb-admin-2.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('js/jquery-ui.js') }}"></script>--}}
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
</html>