<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Edit Servis & Tarif</title>
    <!--Bootstrap-->

    <link href="{{ asset('css/sb-admin-2.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap-theme.css') }}" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('js/html5shiv.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-1.11.3.js') }}"></script>
    <link href="{{ asset('css/font-awesome.css') }}" media="all" rel="stylesheet" type="text/css" />
    {{--    <link href="{{ asset('css/font-awesome.min.css') }}" media="all" rel="stylesheet" type="text/css" />--}}
    {{--<link href="{{ asset('css/metisMenu.min.css') }}" media="all" rel="stylesheet" type="text/css" />--}}
    {{--<link href="{{ asset('css/morris.css') }}" media="all" rel="stylesheet" type="text/css" />--}}

</head>

<body>
@if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('message'))
    <div class="alert alert-success">
        {{Session::get('message')}}
    </div>
@endif

<!-- Display -->

@include('include.sidebar')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Data Servis & Tarif</h1>
            <div class="col-lg-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Input Servis & Tarif
                    </div>
                    <div class="panel-body">
                        <form action="/editTarif/<?php echo $tarif[0]->idservis; ?>" class="form-horizontal" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label for="idservis" class="control-label col-lg-2">Id :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="idservis" id="idservis" disabled value="<?php echo $tarif[0]->idservis; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ketservis" class="control-label col-lg-2">Keterangan :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="ketservis" id="ketservis" value="<?php echo $tarif[0]->ketservis; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tarifservis" class="control-label col-lg-2">Tarif :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="tarifservis" id="tarifservis" value="<?php echo $tarif[0]->tarifservis; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-5">
                                    <hr>
                                    <input type="submit" class="btn btn-primary" value="Update">
                                    &nbsp;&nbsp;
                                    <input type="reset" class="btn btn-danger" value="Reset">
                                </div>
                            </div>

                            <hr>
                        </form>
                    </div> <!-- End panel body -->
                </div> <!-- End panel default -->
            </div>
        </div>
    </div>
</div>

</body>

<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/metisMenu.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sb-admin-2.js') }}"></script>
</html>