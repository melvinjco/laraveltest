<html>
<head>
    <link href="{{ asset('css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap-theme.css') }}" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('js/html5shiv.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-1.11.3.js') }}"></script>
</head>
<body>
<br><br><br>
    <div class="container">
        <div class="col-md-4 col-md-offset-4">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Please Sign Up</h3>
                </div>
                <div class="panel-body">
                    <form action="{{route('signup')}}"  method="post">
                        <div class="form-group {{$errors->has('email') ? 'has-error has-feedback' : ''}}">
                            <input type="text" class="form-control" name="email" id="email" value="{{Request::old('email')}}" autofocus placeholder="E-mail">
                            <span class="{{$errors->has('email') ? 'glyphicon glyphicon-remove form-control-feedback' : ''}}" aria-hidden="true"></span>
                        </div>
                        <div class="form-group {{$errors->has('first_name') ? 'has-error has-feedback' : ''}}">
                            <input type="text" class="form-control" name="first_name" id="first_name" value="{{Request::old('first_name')}}" placeholder="First Name">
                            <span class="{{$errors->has('first_name') ? 'glyphicon glyphicon-remove form-control-feedback' : ''}}" aria-hidden="true"></span>
                        </div>
                        <div class="form-group {{$errors->has('password') ? 'has-error has-feedback' : ''}}">
                            <input type="password" class="form-control" name="password" id="password" value="{{Request::old('password')}}" placeholder="Password">
                            <span class="{{$errors->has('password') ? 'glyphicon glyphicon-remove form-control-feedback' : ''}}" aria-hidden="true"></span>
                        </div>
                        <button type="Submit" class="btn btn-block btn-primary">Submit</button>
                        <input type="hidden" name="_token" value="{{Session::token()}}">
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>