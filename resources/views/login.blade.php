<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Log In</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap-theme.css') }}" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('js/html5shiv.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-1.11.3.js') }}"></script>

</head>

<body>
<br><br><br>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Please Sign In</h3>
                </div>
                <div class="panel-body">
                    <form action="{{route('login')}}" method="post">

                            <div class="form-group">
                                <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                </label>
                            </div>
                            <!-- Log In to Dashboard -->
                            <button class="btn btn-block btn-primary btn-block" value="Submit">Login</button>
                            <input type="hidden" name="_token" value="{{Session::token()}}">
                            <br>
                            <small><a href="{{route('signup')}}">Haven't Account?</a></small>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html>
