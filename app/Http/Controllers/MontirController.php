<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class MontirController extends Controller
{
    public function insertMontir(Request $request){

        $this->validate($request, [
            'idMontir' => 'required|unique:montir',
            'nama' => 'required|max:50'
        ]);

        $idMontir = $request->input('idMontir');
        $nama = $request->input('nama');

        if(DB::insert('insert into montir (idmontir, nama)values(?,?)',[$idMontir, $nama])){
            $message = 'Data Montir Inserted';
        }

        return redirect()->route('entri_montir')->with(['message' => $message]);
    }

    public function index(){
        $montir = DB::table('montir')
                    ->select('*')
                    ->get();
        return view('entri_montir',['montir'=>$montir]);
    }

    public function show($id)
    {
        $montir = DB::select('select * from montir where idmontir = ?',[$id]);
        return view('edit_montir',['montir'=>$montir]);
    }

    public function editMontir(Request $request, $id){

        $this->validate($request, [
            'nama' => 'required|max:50'
        ]);

        // bisa 2 cara untuk input
        // 1
        $nama = $request->input('nama');
        // 2
        //$name = $request->nama;

        DB::update('update montir set nama = ? where idmontir = ?',
            [$nama, $id]);

        echo "Record updated successfully.<br/>";
        echo '<a href="/entri_montir">Click Here</a> to go back.';
    }

    public function deleteMontir($id)
    {
        // with modul DB
        $montir = DB::delete('delete from montir where idmontir = ?', [$id]);

        echo "Record deleted successfully.<br/>";
        echo '<a href="/entri_montir">Click Here</a> to go back.';
    }

    public function lap_upah(Request $request){
        $tgl_awal = $request->input('tgl_awal');
        $tgl_akhir = $request->input('tgl_akhir');

        $lap_upah = DB::select('select m.idmontir, m.nama, ROUND(SUM(0.7 * t.tarifservis)) as jumlahUpah from workorder w
                               join detilwo d
                               on (d.nowo = w.nowo)
                               join tarif t
                               on (t.idservis = d.idservis)
                               join montir m
                               on (m.idmontir = w.idmontir)
                               where w.tgl between ? and ?
                               group by m.idmontir, m.nama', [$tgl_awal, $tgl_akhir]);

        return view('lap_upah', ['lap_upah'=>$lap_upah]);
    }

    public function printSlip(Request $request, $idmontir, $nama, $tgl_awal, $tgl_akhir){

        $printSlip = DB::select('select w.idmontir, w.tgl, w.nowo, t.ketservis, ROUND(0.7 * t.tarifservis) as upah from workorder w
                               join detilwo d
                               on (d.nowo = w.nowo)
                               join tarif t
                               on (t.idservis = d.idservis)
                               join montir m
                               on (m.idmontir = w.idmontir)
                               where w.idmontir = ? AND w.tgl BETWEEN ? AND ?
                               group by w.tgl, w.nowo, t.ketservis, t.tarifservis, w.idmontir', [$idmontir, $tgl_awal, $tgl_akhir]);

        return view('printSlip', ['printSlip'=>$printSlip, 'tgl_akhir'=>$tgl_akhir, 'tgl_awal'=>$tgl_awal, 'nama'=>$nama]);
    }

}
