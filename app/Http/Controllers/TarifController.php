<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TarifController extends Controller
{
    public function insertTarif(Request $request){

        $this->validate($request, [
            'idservis' => 'required|unique:tarif',
            'ketservis' => 'required|max:100',
            'tarifservis' => 'required'
        ]);

        $idServis = $request->input('idservis');
        $ketservis = $request->input('ketservis');
        $tarifservis = $request->input('tarifservis');


        if(DB::insert('insert into tarif (idservis, ketservis, tarifservis)values(?,?,?)',[$idServis, $ketservis, $tarifservis])){
            $message = 'Data Tarif Inserted';
        }

        return redirect()->route('entri_tarif')->with(['message' => $message]);
    }

    public function index(){
        $tarif = DB::select('select * from tarif');
        return view('entri_tarif',['tarif'=>$tarif]);
    }

    public function show($id)
    {
        $tarif = DB::select('select * from tarif where idservis = ?',[$id]);
        return view('edit_tarif',['tarif'=>$tarif]);
    }

    public function editTarif(Request $request, $id){

        $this->validate($request, [
            'ketservis' => 'required|max:100',
            'tarifservis' => 'required'
        ]);

        $ketservis = $request->input('ketservis');
        $tarifservis = $request->input('tarifservis');

        DB::update('update tarif set ketservis = ?, tarifservis = ? where idservis = ?',
            [$ketservis, $tarifservis, $id]);

        echo "Record updated successfully.<br/>";
        echo '<a href="/entri_tarif">Click Here</a> to go back.';
    }

    public function deleteTarif($id)
    {
        $tarif = DB::delete('delete from tarif where idservis = ?', [$id]);

        echo "Record deleted successfully.<br/>";
        echo '<a href="/entri_tarif">Click Here</a> to go back.';
    }
}
