<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class WoController extends Controller
{
    public function index(){

        $wo = DB::select('select max(nowo)+1 as nowo, CURRENT_DATE as tgl from workorder');
        $montir = DB::select('select idmontir, nama from montir');
        $tarif = DB::select('select d.nowo, t.idservis, t.ketservis, t.tarifservis from tarif t
                             join detilwo d
                             on (d.idservis = t.idservis)
                             where d.nowo = (select max(d.nowo) from tarif t
                             				 join detilwo d
                            				 on (d.idservis = t.idservis))');

        return view('entri_wo')->with(['workorder'=>$wo, 'montir'=>$montir, 'tarif'=>$tarif]);
    }

    public function insertDetilWo(Request $request)
    {
        $nowo = $request->input('nowo');
        $idservis = $request->input('idservis');

        DB::insert('insert into detilwo values(?,?)', [$nowo, $idservis]);

        return redirect()->route('entri_wo');
    }

    public function insertWo(Request $request){
        $nowo = $request->input('nowo');
        $tgl = $request->input('tgl');
        $idmontir = $request->input('idmontir');
        $nomotor = $request->input('nomotor');

        DB::insert('insert into workorder values(?,?,?,?)', [$nowo, $tgl, $idmontir, $nomotor]);

        return redirect()->route('entri_wo');
    }

    public function lap_wo(Request $request){
        $tgl_awal = $request->input('tgl_awal');
        $tgl_akhir = $request->input('tgl_akhir');

        $lap_wo = DB::select('select w.nowo, w.tgl, m.nama, w.nomotor, t.ketservis from workorder w
                               join detilwo d
                               on (d.nowo = w.nowo)
                               join tarif t
                               on (t.idservis = d.idservis)
                               join montir m
                               on (m.idmontir = w.idmontir)
                               where w.tgl between ? and ?', [$tgl_awal, $tgl_akhir]);

        return view('lap_wo', ['lap_wo'=>$lap_wo]);
    }

    public function lap_pendapatan(Request $request){
        $tgl_awal = $request->input('tgl_awal');
        $tgl_akhir = $request->input('tgl_akhir');

        $lap_pendapatan = DB::select('select w.nowo, w.tgl, w.nomotor,  SUM(t.tarifservis) as jumlahPendapatan from workorder w
                               join detilwo d
                               on (d.nowo = w.nowo)
                               join tarif t
                               on (t.idservis = d.idservis)
                               where w.tgl between ? and ? 
                               group by w.nowo, w.tgl, w.nomotor', [$tgl_awal, $tgl_akhir]);

        return view('lap_pendapatan', ['lap_pendapatan'=>$lap_pendapatan]);
    }

    public function printDetil($nowo, $tgl_awal, $tgl_akhir){
        $printDetil = DB::select('select w.nowo, w.tgl, w.nomotor, t.ketservis, t.tarifservis 
                               from workorder w
                               join detilwo d
                               on (d.nowo = w.nowo)
                               join tarif t
                               on (t.idservis = d.idservis)
                               where w.tgl between ? and ? AND w.nowo = ?
                               group by w.nowo, w.tgl, w.nomotor, t.ketservis, t.tarifservis', [$tgl_awal, $tgl_akhir, $nowo]);

        return view('printDetil', ['printDetil'=>$printDetil]);
    }

}
