<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// GET
Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

// Montir
Route::get('/entri_montir','MontirController@index')->name('entri_montir');
Route::post('/insertMontir', [
    'uses' => 'MontirController@insertMontir',
    'as' => 'insertMontir'
]);
Route::get('/editMontir/{id}', 'MontirController@show');
Route::post('/editMontir/{id}', 'MontirController@editMontir');
Route::get('/deleteMontir/{id}', 'MontirController@deleteMontir');

// Tarif
Route::get('/entri_tarif', 'TarifController@index')->name('entri_tarif');
Route::post('/insertTarif', [
    'uses' => 'TarifController@insertTarif',
    'as' => 'insertTarif'
]);
Route::get('/editTarif/{id}', 'TarifController@show');
Route::post('/editTarif/{id}', 'TarifController@editTarif');
Route::get('/deleteTarif/{id}', 'TarifController@deleteTarif');

//Workorder
Route::get('/entri_wo', 'WoController@index')->name('entri_wo');
Route::post('/saveServis', [
    'uses' => 'WoController@insertDetilWo',
    'as' => 'saveServis'
]);
Route::post('/saveWo', [
    'uses' => 'WoController@insertWo',
    'as' => 'saveWo'
]);

// Laporan
Route::get('/lap_wo', 'WoController@lap_wo')->name('lap_wo');
Route::get('/lap_upah', 'MontirController@lap_upah')->name('lap_upah');
Route::get('/printSlip/{idmontir}&{nama}&{tgl_awal}&{tgl_akhir}', 'MontirController@printSlip')->name('printSlip');
Route::get('/lap_pendapatan', 'WoController@lap_pendapatan')->name('lap_pendapatan');
Route::get('/printDetil/{nowo}&{tgl_awal}&{tgl_akhir}', 'WoController@printDetil')->name('printDetil');

// User
Route::get('/signup', function () {
    return view('signup');
})->name('signup');

Route::post('/signup', [
    'uses' => 'UserController@postSignUp',
    'as' => 'signup'
]);

Route::post('/login', [
    'uses' => 'UserController@postSignIn',
    'as' => 'login'
]);

Route::get('/login', function () {
    return view('login');
})->name('login');

